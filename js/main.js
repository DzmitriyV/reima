$(document).ready(function () {

    $(".nano").nanoScroller();

    /*Сбрасывание поиска*/

    $("body").on("click", ".form-reset", function() {
        $(this).hide();
    });

    $("#address").on("change input", function() {
        if($(this).val().length > 0) {
            $(".form-reset").show();
        } else {
            $(".form-reset").hide();
        }
    })

    /*Сбрасывание поиска*/
    /*Выпадалка флаги*/

    $("body").on("click", "#dropdownMenu1", function() {
        $("#language-switcher").toggleClass("open");
    });

    $("body").on("click", function(e){
        if(!$("#dropdownMenu1").is(e.target) && !$("#dropdownMenu1").find("img").is(e.target)) {
            $("#language-switcher").removeClass("open");
        }
    });

    /*Выпадалка флаги*/
    /*Выпадалка адреса*/
    $("body").on("click", ".place", function () {
        if($(window).width() > 1024) {
            showAdress1();
        }
        else {
            showAdress2();
        }
    });

    /*Выпадалка адреса*/
    /*Выпадалка  1024*/

    $("body").on("click", "#top-filters .filters", function(e){
        if($(window).width() < 1025) {
            if($(this).hasClass("open")) {
                $(this).removeClass("open");
            } else {
                $(this).addClass("open");
            }
        }
    });

    $("body").on("click", function(e){
        if($(window).width() < 1025) {
            if (!$("#top-filters .filters").is(e.target) && $("#top-filters .filters").has(e.target).length === 0) {
                $("#top-filters .filters").removeClass("open")
            }
        }
    });

    /*Выпадалка  1024*/
    /*Выпадалки мобильная версия*/

    $("body").on("click", "#navbar-mobile .dropdown-toggle", function() {
        if($(this).parent(".header-cell").hasClass("open")) {
            $(this).parent(".header-cell").removeClass("open");
        }
        else {
            $("#navbar-mobile .dropdown-toggle").parent(".header-cell").removeClass("open");
            $(this).parent(".header-cell").addClass("open");
        }
    });

    $("body").on("click", "#navbar-mobile .fa-close", function() {
        $(this).parents(".header-cell").removeClass("open");
    });

    $("body").on("click", function(e) {
        if (!$("#navbar-mobile .dropdown-toggle").parent(".header-cell").is(e.target) && $("#navbar-mobile .dropdown-toggle").parent(".header-cell").has(e.target).length === 0) {
            $("#navbar-mobile .dropdown-toggle").parent(".header-cell").removeClass("open");
        }
    });

    $("body").on("click", "#place-list-mob", function() {
        if($(this).parent(".header-cell").hasClass("open")) {
            $(this).parent(".header-cell").removeClass("open");
            $("#placesPanel").slideUp("normal");
        }
        else {
            $(this).parent(".header-cell").addClass("open");
            $("#placesPanel").slideDown("normal");
        }
    });

    $("body").on("click", "#header-place-list-mobile", function() {
        $("#placesPanel").slideUp("normal");
        $("#place-list-mob").parents(".header-cell").removeClass("open");
    });

    $(window).resize(function() {
        if($(window).width() > 770) {
            $("#placesPanel").show();
        }
        else {
            $("#place-list-mob").parents(".header-cell").removeClass("open");
            $("#placesPanel").hide();
        };
    });

    /*Выпадалки мобильная версия*/
    /*Подсказки*/

    $("body").on("mouseenter", ".btn", function() {
       if($(this).next(".tooltip")) {
           $(this).next(".tooltip").show();
       }
    });
    $("body").on("mouseleave", ".btn", function() {
       if($(this).next(".tooltip")) {
           $(this).next(".tooltip").hide();
       }
    });

    $("body").on("mouseenter", ".placeActions .btn", function() {
        var attr1=$(this).attr("data-tooltip");
        var tooltip=$(".tooltip-left[data-tooltip =" + attr1 + " ]");
        var top1=$(this).offset().top;
        var left1=$(this).offset().left - tooltip.innerWidth();
        tooltip.show().css({top:top1, left:left1});
    });
    $("body").on("mouseleave", ".placeActions .btn", function() {
        $(".tooltip-left").hide();
    });

    $("#rightPlaces").on("scroll", function() {
        $(".tooltip-left").hide();
    })

    $("body").on("mouseenter", "#leftTopButtons .btn", function() {
        var attr1=$(this).attr("data-tooltip");
        var tooltip=$(".tooltip-top[data-tooltip =" + attr1 + " ]");
        var top1=$(this).offset().top - tooltip.innerHeight();
        var left1=$(this).offset().left - (tooltip.innerWidth() - $(this).outerWidth())/2;
        tooltip.show().css({top:top1, left:left1});
    });
    $("body").on("mouseleave", "#leftTopButtons .btn", function() {
        $(".tooltip-top").hide();
    });

    /*Подсказки*/

});

function showAdress1() {

    if (!$(".place").hasClass("open")) {
        $(this).addClass("open");
          $("#placePanel").show("slide", {direction: "right"}, 100);

    }
    else if ($(".place").hasClass("open") && !$(this).hasClass("open")) {
        $(".place").removeClass("open");
        $(this).addClass("open");
    }

    $("body").on("click", ".closeBtn", function () {
        $(".place").removeClass("open");
        $("#placePanel").hide("slide", {direction: "right"}, 100);
    });
};

function showAdress2() {

    if (!$(".place").hasClass("open")) {
        $(this).addClass("open");
        $("#placesPanel").hide();
        $("#placePanel").show("slide", {direction: "right"}, 100);

    }

    $("body").on("click", ".closeBtn", function () {
        $(".place").removeClass("open");
        $("#placesPanel").show();
        $("#placePanel").hide("slide", {direction: "right"}, 100);
    });
};


